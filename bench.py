import flame
import argparse
import pathlib
import functools

assert __name__ == "__main__", "This is a top level script, not a library"
parser = argparse.ArgumentParser()
parser.add_argument("directory", help="OpenLane runs directory")
args = parser.parse_args()


def make_csv_label_column(plot_list):
    output = []
    for time, root, task in plot_list:
        output.append(f"{root}: {task}")
    return output


def make_csv_column(plot_list):
    output = []
    for time, root, task in plot_list:
        output.append(f"{time}")
    return output


run_dir = pathlib.Path(args.directory)
columns = []
label_column = []
for p in run_dir.iterdir():
    split_run_name = p.name.split('_')
    if split_run_name[0] == 'BENCH':
        _, width, depth = split_run_name
        ops = flame.read_ops(p / 'runtime.yaml')
        jobs = list(filter(None, map(flame.transform, next(ops))))
        # plot_list = flame.make_branched(next(ops))
        # print(jobs)
        # asdasd
        columns.append([])
        columns[-1] = [f"{width}"] + [f"{depth}"]
        columns[-1] += make_csv_column(jobs)
        if not label_column:
            label_column = make_csv_label_column(jobs)

# print(len(label_column))
# print(columns[1])
output = ""
output += "width;"
sorted_columns = sorted(columns, key=lambda x: f"{x[0] :0>3} {x[1] :0>3}")
for column in sorted_columns:
    output += f"{column[0]};"
output += "\n"
output += "depth;"
for column in sorted_columns:
    output += f"{column[1]};"
output += "\n"
for i in range(len(label_column)):
    output += f"{label_column[i]};"
    for times in sorted_columns:
        try:
            output += f"{times[i+2]};"
        except IndexError:
            output += ";"
            # print(f"skip {i}")
    output += "\n"
    # output += f"{label_column[i]};seconds\n"
print(output, end="")
# lengths = list(map(len, sorted_columns))
# assert all(x == lengths[0] for x in lengths), "Incoherent logs"
