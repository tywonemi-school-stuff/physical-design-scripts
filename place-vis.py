#!/bin/python

from pathlib import Path
import re
import subprocess
import os

OPENROAD = Path("/home/emil/pulls/OpenROAD")
OPENLANE = Path("/home/emil/pulls/OpenLane")
# PROJECT = Path("/home/emil/pulls/chase-the-beat")
PROJECT = OPENLANE / Path("designs/ctu_can_fd")
# CONFIG_LOC = Path("runs/wokwi/config.tcl")
CONFIG_LOC = Path("runs/RUN_2022.11.29_00.15.36/config.tcl")
OUTPUT = Path("python")

fixed = ""
with open(PROJECT / CONFIG_LOC, 'r') as f:
    for line in f:
        l = line
        l = re.sub('{/work', '{' + str(PROJECT), l)
        l = re.sub(' /work', ' ' + str(PROJECT), l)
        l = re.sub('{/openlane', '{' + str(OPENLANE), l)
        l = re.sub(' /openlane', ' ' + str(OPENLANE), l)
        fixed += l

with open("fixed-config.tcl", 'w') as f:
    f.write(fixed)

with open("place-vis.tcl", 'w') as f:
    fixed_path = Path("fixed-config.tcl").resolve()
    f.write(f"source {fixed_path}\n")
    with open(OPENLANE / "custom-dplace.tcl", 'r') as f2:
        f.write(f2.read())

os.mkdir(OUTPUT)
cmd = [Path(OPENROAD/"build/src/openroad").resolve(),
       Path("place-vis.tcl").resolve(), "-gui", "-exit"]
p = subprocess.Popen(cmd, cwd=OUTPUT)
p.communicate()
pngs = sorted(os.listdir(path=OUTPUT))
print(f"Converting {len(pngs)} PNGs to an animated WEBM...")
subprocess.run(["convert", "-delay", "2"] +
               pngs + ["animated.webm"], cwd=OUTPUT)
print("Done")
